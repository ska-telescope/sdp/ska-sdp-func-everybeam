.. include:: ../README.md
   :parser: myst_parser.sphinx_

.. toctree::
   :maxdepth: 2
   :hidden:
   :titlesonly:
   :caption: Home

   self
   changelog

.. toctree::
   :maxdepth: 2
   :hidden:
   :titlesonly:
   :caption: Getting Started

   build-instructions

.. toctree::
   :maxdepth: 2
   :hidden:
   :titlesonly:
   :caption: Interfaces

   tree/interfaces-overview
   tree/cpp-interface
   tree/python-interface

.. toctree::
   :maxdepth: 2
   :hidden:
   :titlesonly:
   :caption: Examples

   tree/python-demos

.. toctree::
   :maxdepth: 2
   :hidden:
   :titlesonly:
   :caption: Technicalities

   design-2.0
   tree/lobes
   tree/station-hierarchy
   tree/polarization

.. toctree::
   :maxdepth: 2
   :hidden:
   :titlesonly:
   :caption: Indices and Tables

   indices-and-tables